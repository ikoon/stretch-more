/**
 * @file
 * Placeholder file for custom sub-theme behaviors.
 *
 */
(function ($, Drupal) {

  /**
   * Use this behavior as a template for custom Javascript.
   */
  Drupal.behaviors.exampleBehavior = {
    attach: function (context, settings) {
      //alert("I'm alive!");
    }
  };

    /**
     * Use this behavior as a template for custom Javascript.
     */
    Drupal.behaviors.cookies = {
        attach: function (context, settings) {
            $(document).ready(function() {
                $('.decline-button').on('click', function(){
                    $('#sliding-popup').remove();
                });
            });
        }
    };

    /**
     * Use this behavior as a template for custom Javascript.
     */
    Drupal.behaviors.popUp = {
        attach: function (context, settings) {
            $(context).find('#block-popup').once('ifPopUp').each(function () {
                var cookiePopUpCheck = $.cookie("popUp-check");

                function checkCookie() {
                    cookiePopUpCheck = $.cookie("popUp-check", 1, {expires: 2});
                }
                if (!cookiePopUpCheck) {
                    $('#block-popup').foundation('open');
                }

                $('#close-pop-up').on('click', function () {
                    $("#block-popup").on("closed.zf.reveal", function (e) {
                        checkCookie();
                    });
                    $('#block-popup').foundation('close');
                });
            });
        }
    }

    /**
     * Use this behavior as a template for custom Javascript.
     */
    Drupal.behaviors.slides = {
        attach: function (context, settings) {
            $(context).find('.field-name-field-slides.swiper-container').once('ifLatestSlider').each(function (){
                var slides = new Swiper (this, {
                    autoplay: true,
                    effect: 'fade',
                    navigation: {
                        nextEl: '.swiper-button-next',
                        prevEl: '.swiper-button-prev',
                      },
                });
            });

            $(context).find('.field-name-field-images.swiper-container').once('ifLatestSlider').each(function (){
                var slides = new Swiper (this, {
                    autoplay: true,
                    effect: 'fade',
                      navigation: {
                        nextEl: '.swiper-button-next',
                        prevEl: '.swiper-button-prev',
                      },
                });
            });

                $(context).find('.realisation-paragraph.swiper-container').once('ifLatestSlider').each(function (){
                var slides = new Swiper (this, {
                    autoplay: true,
                    effect: 'slide',
                      navigation: {
                        nextEl: '.swiper-button-next',
                        prevEl: '.swiper-button-prev',
                      },
                });
            });
        }
    };

    /**
     * offCanvas behaviors
     */
    Drupal.behaviors.offCanvas = {
        attach: function (context, settings) {
            $(context).find('#offCanvasRightOverlap').once('ifRightCanvas').each(function () {
                $menuButton = $('#icon3');
                $(this).on("opened.zf.offcanvas", function(e){
                    $menuButton.toggleClass('open');
                    $('body,html').addClass('overflow-hidden');
                });
                $(this).on("closed.zf.offcanvas", function(e){
                    $menuButton.removeClass('open');
                    $('body,html').removeClass('overflow-hidden');
                });
            });
        }
    };

    Drupal.behaviors.filterfix = {
      attach: function (context, settings){
        $('.js-form-type-radio').on("click", function(){
          $(this).addClass('active');
          $(this).siblings().removeClass('active');
        });
      }
    };

    Drupal.behaviors.activemenu = {
      attach: function (context, settings){
        $('.number-wrapper').each(function(){
          if($(this).find('a').hasClass('is-active')){
            $(this).addClass('active');
          }else{
            $(this).removeClass('active');
          }
        });
      }
    };

    Drupal.behaviors.fixedmenu = {
      attach: function (context, settings){
        $(window).scroll(function() {
          var scroll = $(window).scrollTop();
          var objectSelect = $(".view-mode-full");
          var objectPosition = objectSelect.offset().top;
          if( scroll > objectPosition ) {
            $('body').addClass('fixed');
          } else {
            $('body').removeClass('fixed');
          }
        });
      }
    };

    Drupal.behaviors.scrolllist = {
      attach: function (context, settings) {
        $(window).scroll(function(){
          var scroll = $(window).scrollTop();
          if ($(".paragraph--type--methods")[0]){
            var objectSelect = $(".paragraph--type--call-to-action").parent();
            var objectPosition = objectSelect.offset().top;
            var objectSelect3 = $(".paragraph--type--methods");
            var objectPosition3 = objectSelect3.offset().top;
            if( scroll > objectPosition ) {
              $('.page-scroll').removeClass('fixed-line');
            } else {
              if(scroll > objectSelect3.outerHeight(true) -75){
                $('.page-scroll').removeClass('fixed-line');
              }else{
                $('.page-scroll').addClass('fixed-line');
              }
            }
          }
        });
      }
    };

})(jQuery, Drupal);
